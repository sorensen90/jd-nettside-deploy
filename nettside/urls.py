from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^timeplan', views.timeplan, name='timeplan'),
    url(r'^stilarter', views.stilarter, name='stilarter'),
    url(r'^pedagoger', views.pedagoger, name='pedagoger'),
    url(r'^priser', views.priser, name='priser'),
    url(r'^forestillinger', views.forestillinger, name='forestillinger'),
    url(r'^kontaktoss', views.kontaktoss, name='kontaktoss'),
]
