from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'nettside/index.html', {})

def timeplan(request):
	return render(request, 'nettside/timeplan.html', {})

# Denne har jeg lagt til og er ikke sikkert at fungerer.
def pedagoger(request):
	return render(request, 'nettside/pedagoger.html', {})

def stilarter(request):
	return render(request, 'nettside/stilarter.html', {})

def priser(request):
	return render(request, 'nettside/priser.html', {})

def forestillinger(request):
	return render(request, 'nettside/forestillinger.html', {})

def kontaktoss(request):
	return render(request, 'nettside/kontaktoss.html', {})